'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var Tabs = require('./ios/Components/tabs.js');
var storeGlobal = require('./ios/Stores/storeGlobal.js');
var actionsGlobal = require('./ios/Stores/actionsGlobal.js');
var Loading = require('./ios/Components/generic/loading.js');

var {
	AppRegistry,
	PushNotificationIOS
} = React;


var mainInit = React.createClass({
	mixins: [Reflux.listenTo(storeGlobal,"onStoreGlobalChange")],

	getInitialState: function() {
		return {
			isReady: false,
			globalData: '',
		}
	},

	componentWillMount: function() {
		PushNotificationIOS.requestPermissions();
		actionsGlobal.initialize(this.props.isDebug);
	},

	onStoreGlobalChange: function(dataGlobal) {
		if (dataGlobal) {
			this.setState({
				isReady: true,
				globalData: dataGlobal,
			})
		}
	},

	render: function() {
		if (this.state.isReady) {
			return (
				<Tabs globalData={this.state.globalData} />
			);
		} else {
			return (
				<Loading />
			);
		}
	},
});

AppRegistry.registerComponent('FrankParkPatrol', () => mainInit);
