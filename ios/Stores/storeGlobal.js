'use strict';

var Reflux = require('reflux');
var ActionsGlobal = require('./actionsGlobal.js');

var GlobalStore = Reflux.createStore({
	listenables: ActionsGlobal,
	globalData: [],

	onInitialize: function(statusApp) {
		// if environment is dev
		if (statusApp) {
			this.globalData['urbanAirshipUser'] = 'A_ldaWhdSVCxzNo0aSCWkg';
			this.globalData['urbanAirshipPassword'] = '2BS0Q2B0RJioP96O7UBagA';
			this.globalData['firebaseDB'] = 'https://dazzling-inferno-9421.firebaseio.com/';
			this.globalData['firebaseControlsPath'] = '/dev/controls/';
		}
		// if environment is prod
		else {
			this.globalData['urbanAirshipUser'] = 'V0OVh6TyRvKo0MZ4evj5sA';
			this.globalData['urbanAirshipPassword'] = 'MwUpmBqSRzG_4e_8vlsq1A';
			this.globalData['firebaseDB'] = 'https://dazzling-inferno-9421.firebaseio.com/';
			this.globalData['firebaseControlsPath'] = '/controls/';
		}

		this.trigger(this.globalData);
	},

	onGetData: function() {
		this.trigger(this.globalData);
	}
});

module.exports = GlobalStore;
