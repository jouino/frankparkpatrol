var Reflux = require('reflux');

var ActionsGlobal = Reflux.createActions([
	'initialize',
  'getData'
]);

module.exports = ActionsGlobal;
