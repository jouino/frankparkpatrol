'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var ActionsControls = require('./actionsControls.js');
var storeGlobal = require('../Stores/storeGlobal.js');
var firebase = require('firebase');

var ControlsStore = Reflux.createStore({
	listenables: ActionsControls,

	listControls: [],
	nbControls: 0,
	globalData: [],

	init: function() {
		this.listenTo(storeGlobal, this.globalVariablesChanged);
	},

	globalVariablesChanged: function(data) {
		if (data) {
			this.globalData = data;
			this.listenChildAdded();
		}
	},

	getUsername: function(index) {
		new Firebase(this.globalData['firebaseDB']).child("users/" + this.listControls[index].control.user).once('value', (function(snapUser) {
			if (snapUser.val().username) {
				this.listControls[index].control.username = snapUser.val().username;
				if (index+1 == this.nbControls) {
					setTimeout((function() {
						this.trigger(this.listControls);
					}.bind(this)),0)
				};
			};
		}.bind(this)));
	},

	onGetUsername: function() {
		for (var i = 0; i < this.listControls.length; i++) {
			this.getUsername(i);
		}
	},

	// addControl action
	onAddControl: function(locationControl,dateControl,statusControl,markTimeControl,userID,invertTimestamp) {
		new Firebase(this.globalData['firebaseDB']).child(this.globalData['firebaseControlsPath']).push().set({
			location: locationControl,
			date: new Date(dateControl).getTime(),
			status: statusControl,
			markTime: markTimeControl,
			invertTimestamp: invertTimestamp,
			user: userID,
		});

		if (statusControl == 'Unclear') {
			let urlUrbanFetch = 'https://'+ this.globalData['urbanAirshipUser'] +':'+ this.globalData['urbanAirshipPassword'] +'@go.urbanairship.com/api/push/';

			fetch(urlUrbanFetch, {
				method: 'POST',
				headers: {
					'Accept': 'application/vnd.urbanairship+json; version=3',
					'Content-Type': 'application/json',
					Authorization: 'Basic <master authorization string>'
				},
				body: JSON.stringify({
					"audience": "all",
					"notification": {
						"alert": "A car has been marked near to the office!",
					},
					"device_types": "all"
				})
			}).then((response) => response.text())
			.then((responseText) => {
				console.log(responseText);
			})
			.catch((error) => {
				console.warn(error);
			});
		}
	},

	listenChildAdded: function() {
		new Firebase(this.globalData['firebaseDB']).child(this.globalData['firebaseControlsPath']).orderByChild("invertTimestamp").limitToLast(10).on("child_added", (function(snapControl) {
			this.listControls.push({id: snapControl.key(), control: snapControl.val()});
			this.nbControls++;
			this.getUsername((this.listControls.length)-1);
		}.bind(this)), function (errorObject) {
			console.log("The read failed: " + errorObject.code);
		});
	},
});

module.exports = ControlsStore;
