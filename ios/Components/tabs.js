'use strict';

var React = require('react-native');
var TabTimeline = require('./timelineTab/tab.js');
var TabAddControl = require('./controlsTab/tab.js');
var TabSettings = require('./settingsTab/tab.js');

var {
	StyleSheet,
	TabBarIOS,
	Text,
	View,
} = React;

var iconTimeline = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAARCAYAAADQWvz5AAAAAXNSR0IArs4c6QAAANVJREFUOBHNVDEOgzAMdCI2JPqB8qB+oGO7pVJWxNaxW3aGPKHfgg80EivBV5WIDIhQFiyZM45znBMLUVXVRUppiejMvtm89y37I9tDgq8KIUp2m3H8VWKMEUqpU1EUV+fc21r7SZFX17UHh5wXgwTswHk+JYaiYFDCJAQMycRA/KQlli+XRa0tl62vhNZw2FrrMs/zV9/3z6Zp2vXtRFNHgQibQMJwY8TrHY9Ui4igBCTAVIKp7uCHvWOyKbr+w0w25uWvX8h0Y4ydHIZBIZglt4YdOEZSaVnwUB1p9wAAAABJRU5ErkJggg==';
var iconAddControl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAPZJREFUSA1jYKAiKC0t/Q/CyEYyIXNowaa5BYyUuho9SNDNo7kPaG4Buo8o4mNLRSzFxcWeTExMs4Amy5Bq+v///x8DcXpvb+92XHqBZpNnOMhARkZGWah+XOYzsABlwC7v7u4mOUVBUxDc59jMoHkkj1qAM3JhEqNBBAsJnDTNgwiU0Z4AsQxysQvKMMAiRAuYU1cDsRZO50EkQPpxAqZ///6lAWVRFAENjwcWAaeJMRyqH6cFKMUDsi9AOoAF2cJPnz5lzZo16xtOEwhIgIIIGwAZmN3T07MAmyQpYhgWAF19DYhDgUXwNVIMwqUWxQJqBAm6RQAIf1XMTajojAAAAABJRU5ErkJggg==';
var iconSettings = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAASCAYAAABb0P4QAAAAAXNSR0IArs4c6QAAAPpJREFUOBFjZCATODg4sJiYmLQDtScwMjL++v///+KHDx/WsZBpHoOxsXEr0KASmH4gu1xeXp6BsbS09D9MkBwa6DIfkD6ggVuA7OdM5BiCrOffv38oDmJEliSFXVxc3MnExFSGrAfowk5mZAFS2Ozs7AekpKS4gHrUgPgjEE8DRQqGC6Gx1wZUkAjEIDD/zJkzVQcOHPgD4eInMcIQGnulwEAWgeJSkBh+YxCyOGMZOfYQygmzMFwI04IeezBxQjRGGGKLPaDhXb29veWEDAPJY8QyWux9A6qZdvbs2aoHDx78I8ZADBcSowmkZjQvw0OK7DDElhpokpcBbHB1SLdDza0AAAAASUVORK5CYII=';

var TabBar = React.createClass({
	propTypes: {
		globalData: React.PropTypes.array
	},

	getInitialState: function() {
		return {
			selectedTab: 'timelineTab',
		};
	},

	render: function() {
		return (
			<TabBarIOS tintColor="#fff" barTintColor="#E10C0C">
				{/* Timeline tab */}
				<TabBarIOS.Item
				title="Feed"
				icon={{uri: iconTimeline, scale: 1}}
				selected={this.state.selectedTab === 'timelineTab'}
				onPress={() => {
					this.setState({
						selectedTab: 'timelineTab',
					});
				}}>
					<TabTimeline />
				</TabBarIOS.Item>

				{/* Add control tab */}
				<TabBarIOS.Item
				title="Add"
				icon={{uri: iconAddControl, scale: 1}}
				selected={this.state.selectedTab === 'addControlTab'}
				onPress={() => {
					this.setState({
						selectedTab: 'addControlTab',
					});
				}}>
					<TabAddControl/>
				</TabBarIOS.Item>

				{/* Settings tab */}
				<TabBarIOS.Item
				title="Settings"
				icon={{uri: iconSettings, scale: 1}}
				selected={this.state.selectedTab === 'settingsTab'}
				onPress={() => {
					this.setState({
						selectedTab: 'settingsTab',
					});
				}}>
					<TabSettings/>
				</TabBarIOS.Item>
			</TabBarIOS>
		);
	},

});


module.exports = TabBar;
