var Reflux = require('reflux');

var ActionsControls = Reflux.createActions([
    'addControl',
    'getControlsList',
    'getUsername'
]);

module.exports = ActionsControls;
