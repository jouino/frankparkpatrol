'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var ActionsUser = require('../actionsUser.js');
var ActionsControls = require('../actionsControls.js');
var StoreControls = require('../storeControls.js');
var Loading = require('../generic/loading.js');
var Header = require('../generic/header.js');

var {StyleSheet,Text,View,ListView} = React;

var TabTimeline = React.createClass({
	mixins: [Reflux.listenTo(StoreControls,'onControlsListUpdate')],

	onControlsListUpdate: function(data) {
		var tempData = [];
		this.setState({dataSource: this.state.dataSource.cloneWithRows(tempData)});
		for (var i = data.length -1; i >= 0; i--) {
			tempData.push(data[i]);
		}

		this.setState({dataSource: this.state.dataSource.cloneWithRows(tempData)});
	},

	getInitialState: function() {
		return {
			dataSource: new ListView.DataSource({
				rowHasChanged: (row1, row2) => row1 !== row2,
			}),
		};
	},

	render: function() {
		if (this.state.dataSource._cachedRowCount == 0) {
			return (
				<Loading />
			);
		}
		if (this.state.dataSource._cachedRowCount != 0) {
			return (
				<ListView
				dataSource={this.state.dataSource}
				renderRow={this.renderEvents}
				style={styles.listView}
				renderHeader={this.renderHeader}
				renderSeparator={this.renderSeparator}
				/>
			);
		}
	},

	renderEvents: function(event) {
		var dateEvent = new Date(event.control.date);
		var statusColor = styles.rowStatusColor_clear;
		if (event.control.status == 'Unclear') {
			statusColor = styles.rowStatusColor_unclear;
			if (event.control.markTime) {
				var timeMarked = new Date(event.control.markTime);
			}
		};
		return (
			<View style={styles.rowContainer} key={event.id}>
				<View style={[styles.rowStatusColor, statusColor]} />
				<View style={styles.rowContent}>
					<Text style={styles.rowContent__details}>
						<Text style={[{fontWeight: 'bold'}]}>{('0' + dateEvent.getDate()).slice(-2)}/{('0' + dateEvent.getMonth()).slice(-2)}/{dateEvent.getFullYear()} </Text>
						{this.formatAMPM(dateEvent)} - {event.control.location}
					</Text>
					<Text style={styles.rowContent__infos}>
						{ event.control.status == 'Unclear' ?
						event.control.markTime ?
						<Text>Marked at {this.formatAMPM(timeMarked)}. </Text>
						:
						<Text>No mark. </Text>
						: null }
						Checked by {event.control.username}
					</Text>
				</View>
			</View>
		);
	},

	renderHeader: function() {
		return (
			<Header title="Frank Park Patrol" />
		);
	},

	renderSeparator: function(sectionID, rowID) {
		return (
			<View key={rowID} style={styles.separator}></View>
		)
	},

	formatAMPM: function(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12;
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	},
});

var styles = StyleSheet.create({
	// Header
	header_container: {
		height: 50,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#E10C0C'
	},
	header_title: {
		color: '#fff',
		fontSize: 18,
	},
	// Global list
	title: {
		fontSize: 20,
		marginBottom: 8,
		textAlign: 'center',
	},
	listView: {
		backgroundColor: '#fff',
		marginTop: 20,
		marginBottom: 50,
	},
	separator: {
		height: 1,
		backgroundColor: '#C4C4C4',
		flex: 1,
	},
	// Row
	rowContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		paddingTop: 15,
		paddingBottom: 15,
		paddingLeft: 15,
		paddingRight: 15,
	},
	rowStatusColor: {
		borderRadius: 20,
		width: 40,
		height: 40,
	},
	rowStatusColor_clear: {
		backgroundColor: '#417505',
	},
	rowStatusColor_unclear: {
		backgroundColor: '#D0021B',
	},
	rowContent: {
		flex: 1,
		paddingLeft: 15,
		flexDirection: 'column',
	},
	rowContent__infos: {
		fontSize: 12,
	}
});

module.exports = TabTimeline;
