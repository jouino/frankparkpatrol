'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var UserStore = require('../storeUser.js');
var ActionsUser = require('../actionsUser.js');
var NotLoggedInView = require('../generic/notLoggedInView.js');
var LoggedInView = require('./loggedInView.js');
var Loading = require('../generic/loading.js');

var {
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableHighlight,
	AsyncStorage
} = React;

var TabAddControl = React.createClass({
	mixins: [Reflux.listenTo(UserStore, 'onUserUpdate')],

	getInitialState: function() {
		return {
			authData: '',
		};
	},

	onUserUpdate: function(data) {
		if (data) {
			this.setState({
				authData: data
			});
		}
	},

	componentWillMount: function() {
		ActionsUser.currentStatus();
	},

	render: function() {
		if (this.state.authData == 'anonymous') {
			return (
				<NotLoggedInView/>
			)
		}
		if (typeof(this.state.authData) == 'object' && this.state.authData != null ) {
			return (
				<LoggedInView authData={this.state.authData}/>
			)
		}
		return (
			<Loading />
		);
	},
});

var styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},

	inputField: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
	}
});

module.exports = TabAddControl;
