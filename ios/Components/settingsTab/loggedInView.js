'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var UserStore = require('../storeUser.js');
var ActionsUser = require('../actionsUser.js');
var ActionsControls = require('../actionsControls.js');
var Header = require('../generic/header.js');
var ButtonCustom = require('../generic/button.js');

var {
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableHighlight,
	Dimensions,
	Alert,
	PushNotificationIOS,
	Button,
	AlertIOS
} = React;

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

var LoggedInView = React.createClass({
	propTypes: {
		authData: React.PropTypes.object
	},

	getInitialState: function() {
		return {
			usernameTemp: this.props.authData.username,
		};
	},

	componentWillReceiveProps: function(nextProps) {
		if (nextProps.authData.username && this.state.unclearButtonUsername) {
			this.setState({
				unclearButtonUsername: false,
			});
			Alert.alert('Settings','Username changed! :)');
			ActionsControls.getUsername();
		}
	},

	onPressSubmitLogout: function() {
		ActionsUser.logout();
	},

	onPressSubmitUsername: function() {
		if (this.state.usernameTemp != this.props.authData.username) {
			this.setState({unclearButtonUsername:true});
			ActionsUser.defineUsername(this.state.usernameTemp);
		}
	},

	render: function() {
		return (
			<View style={styles.container}>
				<Header title={"Hey "+this.props.authData.username} style={styles.header} />
				<View style={styles.mainContainer}>
					<Text style={styles.textInfo}>You have the possibility to change your username.</Text>
					<TextInput
						onChangeText={(usernameTemp) => this.setState({usernameTemp})}
						value={this.state.usernameTemp}
						style={styles.inputField}
					/>

					<View style={styles.button_container}>
						<ButtonCustom text={!this.state.unclearButtonUsername ? 'Change username' : 'Please Wait'} action={this.onPressSubmitUsername} unclear={this.state.unclearButtonUsername}/>
					</View>

					<View style={[styles.button_container, styles.button_container_logout]}>
						<ButtonCustom text='Log out' action={this.onPressSubmitLogout} style='secondary' />
					</View>

				</View>
			</View>

		);
	},

});

var styles = StyleSheet.create({
	container: {
		height: deviceHeight,
		marginTop: 20,
		marginBottom: 50,
	},

	mainContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 0,
		marginBottom: 50,
	},

	textInfo: {
			marginBottom: 10,
	},

	inputField: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		width: deviceWidth - 30,
		marginLeft: 15,
		marginRight: 15,
		paddingLeft: 15,
	},

	button_container: {
		paddingTop: 20,
		width: deviceWidth - 30,
	},

	button_container_logout: {
		paddingTop: 50,
		width: deviceWidth - 30,
	}
});

module.exports = LoggedInView;
