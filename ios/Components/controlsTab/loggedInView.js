'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var Picker = require('../generic/picker.js');
var TimePicker = require('../generic/datepicker.js');
var Button = require('../generic/button.js');
var Header = require('../generic/header.js');
var ActionsControls = require('../actionsControls.js');
var StoreControls = require('../storeControls.js');

var {
	StyleSheet,
	Text,
	View,
	TouchableHighlight,
	Animated,
	Dimensions,
	DatePickerIOS,
	Alert
} = React;

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;
var streets = [{index: 'Talfourd St'},{index: 'Malborough St'},{index: 'Lombard St'}];
var status = [{index: 'Clear'},{index: 'Unclear'}];

var LoggedInView = React.createClass({
	mixins: [Reflux.listenTo(StoreControls,'onControlAdded')],

	propTypes: {
		authData: React.PropTypes.object
	},

	getInitialState: function() {
		return {
			offSet: new Animated.Value(deviceHeight),
			timeZoneOffsetInHours: (-1) * (new Date()).getTimezoneOffset() / 60,

			modalLocation: false,
			modalTime: false,
			modalMarkTime: false,
			modalStatus: false,

			firstOpenLocation: false,
			firstOpenStatus: false,
			firstOpenTime: false,
			firstOpenMarkTime: false,

			streetIndex: 0,
			street: '',
			streets: streets,

			statusIndex: 0,
			statu: '',
			status: status,

			date: new Date(),
			dateMark: new Date(),

			unclearButton: false,

			inverTimestamp: Date.now(),
		}
	},

	onControlAdded: function(statusControlAdded) {
		if (this.state.unclearButton) {
			var controlToCheck = statusControlAdded[statusControlAdded.length-1];
			if (controlToCheck.control.invertTimestamp == this.state.inverTimestamp && controlToCheck.control.user == this.props.authData.uid) {
				Alert.alert('New check','Added with success, thank you! :)');
				this.replaceState(this.getInitialState());
			} else {
				Alert.alert('New check','Something went wrong, please try again :(');
			}
		};
	},

	changeStreet: function(street) {
		this.setState({
			street,
			streetIndex: street
		})
	},

	changeStatus: function(statu) {
		this.setState({
			statu,
			statusIndex: statu
		});
	},

	onDateChange: function(date) {
		this.setState({date: date});

	},

	onDateMarkChange: function(date) {
		this.setState({dateMark: date});
	},

	validateData: function() {
		if(this.state.firstOpenLocation&&this.state.firstOpenStatus&&this.state.firstOpenTime) {
			this.setState({unclearButton:true});
			if (this.state.firstOpenMarkTime) {
				var timeMarked = new Date(this.state.dateMark).getTime();
			} else {
				var timeMarked = false;
			}
			ActionsControls.addControl(streets[this.state.streetIndex].index,this.state.date,status[this.state.statusIndex].index,timeMarked,this.props.authData.uid,this.state.inverTimestamp);
		}
	},

	render: function() {
		return (
			<View style={styles.container}>
				<Header title="Add a control" subtitle="Follow those steps to add a new control" />

				<View style={styles.step_container}>
					<TouchableHighlight underlayColor="transparent" onPress={ () => this.setState({modalLocation: true}) }>
						<View style={styles.step_button}>
							<Text style={styles.step_title}>Location</Text>
							<Text style={styles.step_link}> - { this.state.firstOpenLocation ? 'Change' : 'Define' } location</Text>
						</View>
					</TouchableHighlight>
					<View>
						{ this.state.firstOpenLocation ?  <Text style={styles.step_selection}>Selected: {streets[this.state.streetIndex].index}</Text> : null}
					</View>
				</View>

				<View style={styles.step_container}>
					<TouchableHighlight underlayColor="transparent" onPress={ () => this.setState({modalTime: true}) }>
						<View style={styles.step_button}>
							<Text style={styles.step_title}>Time</Text>
							<Text style={styles.step_link}> - { this.state.firstOpenTime ? 'Change' : 'Define' } time</Text>
						</View>
					</TouchableHighlight>
					<View>
						{ this.state.firstOpenTime ?  <Text style={styles.step_selection}>Selected: {this.state.date.toLocaleTimeString('en-US',{ hour: '2-digit', minute:'2-digit' })}</Text> : null}
					</View>
				</View>

				<View style={styles.step_container}>
					<TouchableHighlight underlayColor="transparent" onPress={ () => this.setState({modalStatus: true}) }>
						<View style={styles.step_button}>
							<Text style={styles.step_title}>Status</Text>
							<Text style={styles.step_link}> - { this.state.firstOpenStatus ? 'Change' : 'Define' } status</Text>
						</View>
					</TouchableHighlight>
					<View>
						{ this.state.firstOpenStatus ?  <Text style={styles.step_selection}>Selected: {status[this.state.statusIndex].index}</Text> : null}
					</View>
				</View>

				{ this.state.statu == 1 ?
					<View style={styles.step_container}>
						<TouchableHighlight underlayColor="transparent" onPress={ () => this.setState({modalMarkTime: true}) }>
							<View style={styles.step_button}>
								<Text style={styles.step_title}>Time Mark</Text>
								<Text style={styles.step_link}> - { this.state.firstOpenMarkTime ? 'Change' : 'Define' } time</Text>
							</View>
						</TouchableHighlight>
						<View>
							{ this.state.firstOpenMarkTime ?  <Text style={styles.step_selection}>Selected: {this.state.dateMark.toLocaleTimeString('en-US',{ hour: '2-digit', minute:'2-digit' })}</Text> : <Text style={styles.step_selection}>If a tire was marked, please select the time indicated</Text>}
						</View>
					</View>
				: null }

				<View style={styles.button_container}>
					<Button text={!this.state.unclearButton ? 'Validate' : 'Please Wait'} action={this.validateData} unclear={this.state.unclearButton}/>
				</View>

				{ this.state.modalMarkTime ? <View style={styles.modalContainer}><TimePicker closeModal={() => this.setState({ modalMarkTime: false, firstOpenMarkTime: true })} offSet={this.state.offSet} date={this.state.dateMark} timeZoneOffsetInHours={this.state.timeZoneOffsetInHours} changeTime={this.onDateMarkChange} /></View> : null }
				{ this.state.modalTime ? <View style={styles.modalContainer}><TimePicker closeModal={() => this.setState({ modalTime: false, firstOpenTime: true })} offSet={this.state.offSet} date={this.state.date} timeZoneOffsetInHours={this.state.timeZoneOffsetInHours} changeTime={this.onDateChange} /></View> : null }
				{ this.state.modalLocation ? <View style={styles.modalContainer}><Picker closeModal={() => this.setState({ modalLocation: false, firstOpenLocation: true })} offSet={this.state.offSet} streets={this.state.streets} changeTime={this.changeStreet} street={this.state.street} /></View> : null }
				{ this.state.modalStatus ? <View style={styles.modalContainer}><Picker closeModal={() => this.setState({ modalStatus: false, firstOpenStatus: true })} offSet={this.state.offSet} streets={this.state.status} changeTime={this.changeStatus} street={this.state.statu} /></View> : null }
			</View>
		);
	}
});



var styles = StyleSheet.create({
	// Main container
	container: {
		marginTop: 20,
		marginBottom: 50,
	},

	// Step
	step_container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'flex-start',
		backgroundColor: '#fff',
		marginLeft: 15,
		marginRight: 15,
		paddingTop: 20,
		paddingBottom: 20,
		borderBottomColor: '#E4E4E4',
		borderBottomWidth: 1,
	},
	step_button: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'flex-end',
	},
	step_title: {
		color: '#000',
		fontSize: 18,
	},
	step_link: {
		color: '#4A90E2',
		fontSize: 12,
	},
	step_selection: {
		fontSize: 12,
		paddingTop: 5,
	},
	//Validate button
	button_container: {
		marginLeft: 15,
		marginRight: 15,
		paddingTop: 20,
	},
	// Modal
	modalContainer: {
		position: 'absolute',
		top: 0,
		height: deviceHeight,
		width: deviceWidth,
		backgroundColor: 'rgba(255, 255, 255, 0.9)',
	},
});

module.exports = LoggedInView;
