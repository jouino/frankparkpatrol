var Reflux = require('reflux');

var ActionsUser = Reflux.createActions([
	'currentStatus',
	'logout',
	'login',
	'defineUsername',
	'getUsername'
]);

module.exports = ActionsUser;
