'use strict';

var React = require('react-native');

var {
	StyleSheet,
	Text,
	View,
	TouchableHighlight,
	PickerIOS,
	Animated,
	Dimensions
} = React;

var PickerItemIOS = PickerIOS.Item;
var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

var Picker = React.createClass({
	componentDidMount: function() {
		Animated.timing(this.props.offSet, {
			duration: 300,
			toValue: 100
		}).start()
	},

	closeModal() {
		Animated.timing(this.props.offSet, {
			duration: 300,
			toValue: deviceHeight
		}).start(this.props.closeModal);
	},

	render() {
		return (
			<Animated.View style={[styles.container, { transform: [{translateY: this.props.offSet}] }]}>
				<View style={styles.closeButtonContainer}>
					<TouchableHighlight onPress={ this.closeModal } underlayColor="transparent" style={styles.closeButton}>
						<Text style={styles.closeButtonText}>Choose</Text>
					</TouchableHighlight>
				</View>
				<PickerIOS
				selectedValue={this.props.street}
				onValueChange={(index) => this.props.changeTime(index)}>
				{Object.keys(this.props.streets).map((index) => (
					<PickerItemIOS
					key={index}
					value={index}
					label={this.props.streets[index].index}
					/>
				))}
				</PickerIOS>
			</Animated.View>
		)
	}
});

var styles = StyleSheet.create({
	container: {
        position: 'absolute',
        bottom: 150,
        width: deviceWidth,
    },
	closeButtonContainer: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		borderTopColor: '#e2e2e2',
		borderTopWidth: 1,
		borderBottomColor: '#e2e2e2',
		borderBottomWidth:1
	},
	closeButton: {
		paddingRight:10,
		paddingTop:10,
		paddingBottom:10
	},
	closeButtonText: {
		color: '027afe'
	},

});

module.exports = Picker;
