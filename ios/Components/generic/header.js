'use strict';

var React = require('react-native');

var {
	StyleSheet,
	Text,
	View,
} = React;

var Header = React.createClass({
	getDefaultProps: function() {
		return {
			title: false,
			subtitle: false,
		};
	},

	render: function() {
		return (
			<View>
				{ this.props.title ?
					<View style={styles.header_container}>
						<Text style={styles.header_title}>{this.props.title}</Text>
					</View>
				: null }
				{ this.props.subtitle ?
					<View style={styles.subheader_container}>
						<Text style={styles.subheader_title}>{this.props.subtitle}</Text>
					</View>
				: null }
			</View>
		);
	}
});

var styles = StyleSheet.create ({
	header_container: {
		height: 50,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#E10C0C',
	},
	header_title: {
		color: '#fff',
		fontSize: 18,
	},
	subheader_container: {
		height: 60,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#E4E4E4',
	},
	subheader_title: {
		color: '#000',
		fontSize: 14,
	},
});

module.exports = Header;
