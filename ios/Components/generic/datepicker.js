'use strict';

var React = require('react-native');

var {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    DatePickerIOS,
    Animated,
    Dimensions
} = React;

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

var TimePicker = React.createClass({
    componentDidMount: function() {
        Animated.timing(this.props.offSet, {
            duration: 300,
            toValue: 100
        }).start()
    },

    closeModal() {
        Animated.timing(this.props.offSet, {
            duration: 300,
            toValue: deviceHeight
        }).start(this.props.closeModal);
    },

    render() {
        return (
            <Animated.View style={[styles.container, { transform: [{translateY: this.props.offSet}] }]}>
                <View style={styles.closeButtonContainer}>
                    <TouchableHighlight onPress={ this.closeModal } underlayColor="transparent" style={styles.closeButton}>
                        <Text style={styles.closeButtonText}>Choose</Text>
                    </TouchableHighlight>
                </View>
                <DatePickerIOS
                    style={styles.datepicker}
                    date={this.props.date}
                    mode="time"
                    timeZoneOffsetInMinutes={this.props.timeZoneOffsetInHours * 60}
                    onDateChange={this.props.changeTime}
                />
            </Animated.View>
        )
    }
});

var styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 150,
        width: deviceWidth,
    },
    datepicker: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    closeButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        borderTopColor: '#e2e2e2',
        borderTopWidth: 1,
        borderBottomColor: '#e2e2e2',
        borderBottomWidth:1,
    },
    closeButton: {
        paddingRight:10,
        paddingTop:10,
        paddingBottom:10
    },
    closeButtonText: {
        color: '027afe'
    },

});

module.exports = TimePicker;
