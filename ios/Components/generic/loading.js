'use strict';

var React = require('react-native');

var {
	StyleSheet,
	Text,
	View,
	Dimensions,
	Image
} = React;


var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

var Loading = React.createClass({
	render: function() {
		return (
			<View style={styles.container}>
				<Image
					style={styles.gif}
					source={require('../../../assets/loader.gif')}
				/>
			</View>
		);
	}
});

var styles = StyleSheet.create ({
	container: {
		backgroundColor: '#fff',
		height: deviceHeight,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	gif: {
		// flex: 1,
		height: 40,
		width: 40

	}
});

module.exports = Loading;
