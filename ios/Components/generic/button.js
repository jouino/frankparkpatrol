'use strict';

var React = require('react-native');

var {
	StyleSheet,
	Text,
	View,
	TouchableHighlight,
} = React;

var Button = React.createClass({
	render: function() {
		return (
			<TouchableHighlight style={[styles.buttonContainer, this.props.style == 'secondary' ? styles.buttonContainerSecondary : null, this.props.unclear ? styles.buttonContainerUnclear : null]} onPress={ !this.props.unclear ? this.props.action : null}>
				<Text style={styles.buttonText}>{this.props.text.toUpperCase()}</Text>
			</TouchableHighlight>
		);
	}
});

var styles = StyleSheet.create ({
	buttonContainer: {
		backgroundColor: '#60A119',
		flex: 1,
		height: 40,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonContainerSecondary: {
		backgroundColor: 'red',
	},
	buttonContainerUnclear: {
		backgroundColor: '#B8E986'
	},
	buttonText: {
		fontWeight: 'bold',
		fontSize: 16,
		color: '#fff'
	}
});

module.exports = Button;
