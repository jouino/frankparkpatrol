'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var ActionsUser = require('../actionsUser.js');
var StoreUser = require('../storeUser.js');
var Button = require('../generic/button.js');
var Header = require('../generic/header.js');

var {
	StyleSheet,
	Text,
	View,
	TextInput,
	Alert,
	Dimensions
} = React;

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

var NotLoggedInView = React.createClass({
	mixins: [Reflux.listenTo(StoreUser,'onUpdateUser')],

	getInitialState: function() {
		return {
			username: '',
			password: '',
			unclearButton: false,
		};
	},

	onUpdateUser: function() {
		if (this.state.unclearButton) {
			Alert.alert('Authentification Error','Invalid email address or password!');
			this.setState({
				unclearButton: false,
			});
		}
	},

	render: function() {
		return (
			<View style={styles.container}>
				<Header title="Log in" />
				<View style={styles.mainContainer}>
					<Text style={styles.textInfo}>Please log in to get the possibility to add a control</Text>
					<TextInput
						onChangeText={(username) => this.setState({username})}
						value={this.state.username}
						style={styles.inputField}
						placeholder="Email address"
					/>
					<TextInput
						onChangeText={(password) => this.setState({password})}
						value={this.state.password}
						style={styles.inputField}
						secureTextEntry={true}
						placeholder="Password"
					/>
					<View style={styles.button_container}>
						<Button text={!this.state.unclearButton ? 'Log in' : 'Please Wait'} action={this.validateData} unclear={this.state.unclearButton}/>
					</View>
				</View>
			</View>
			);
	},


	validateData: function() {
		if (this.state.username && this.state.password) {
			ActionsUser.login(this.state.username,this.state.password);
			this.setState({ unclearButton: true});
		} else {
			Alert.alert('Authentification Error','Please fill both fields!');
		}
	}
});

var styles = StyleSheet.create({
	container: {
			height: deviceHeight,
			marginTop: 20,
			marginBottom: 50,
	},

	mainContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 15,
		marginRight: 15,
		marginBottom: 70
	},

	textInfo: {
			marginBottom: 10,
	},

	inputField: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		marginTop: -1,
		paddingLeft: 15,
		paddingRight: 15,
	},

	button_container: {
		marginLeft: 15,
		marginRight: 15,
		paddingTop: 20,
		width: deviceWidth - 30,
	}
});

module.exports = NotLoggedInView;
