'use strict';

var React = require('react-native');
var Reflux = require('reflux');
var ActionsUser = require('./actionsUser.js');
var firebase = require('firebase');

var firebaseDB = new Firebase("https://dazzling-inferno-9421.firebaseio.com/");

var {
	AsyncStorage
} = React;


var UserStore = Reflux.createStore({
	listenables: ActionsUser,
	authDataTemp: '',

	init: function() {
		AsyncStorage.getItem('authToken').then((value) => {
			if (value) {
				firebaseDB.authWithCustomToken(value, (function(error, authDataTemp) {
					if (!error) {
						this.authDataTemp = authDataTemp;
						this.onGetUsername();
					}
				}.bind(this)));
			} else {
				this.authDataTemp = 'anonymous';
				this.trigger(this.authDataTemp);
			};
		}).done();
	},

	onGetUsername: function() {

		if (!this.authDataTemp.username) {
			firebaseDB.child('users').child(this.authDataTemp.uid).once("value", (function(snapshot) {
				this.authDataTemp.username = snapshot.val().username;
				this.trigger(this.authDataTemp);
			}.bind(this)), function (errorObject) {
			});
		};
	},

	onCurrentStatus: function() {
		this.trigger(this.authDataTemp);
	},

	onLogout: function() {
		firebaseDB.unauth();
		AsyncStorage.removeItem('authToken');
		this.authDataTemp = 'anonymous';
		this.trigger(this.authDataTemp);
	},

	onLogin: function(username,password) {
		firebaseDB.authWithPassword({
			email: username,
			password: password
		}, (function(error, Data) {
			if (!error) {
				this.authDataTemp = Data;
				this.onGetUsername();
				try {
					AsyncStorage.removeItem('authToken', function() {
						AsyncStorage.setItem('authToken', Data.token);
					});
				} catch (error) {
				}
			} else {
				this.trigger(this.authDataTemp);
			}
		}.bind(this)));
	},

	onDefineUsername: function(newUsername) {
		var userRef = firebaseDB.child("users").child(this.authDataTemp.uid);
		userRef.update({
			"username": newUsername
		}, (function(error) {
			if (error) {
				this.trigger(this.authDataTemp);
			} else {

				this.authDataTemp.username = newUsername;
				this.trigger(this.authDataTemp);
			}
		}.bind(this)));
	}
});

module.exports = UserStore;
